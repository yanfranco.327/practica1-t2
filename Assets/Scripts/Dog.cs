using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Dog : MonoBehaviour
{
    
    private int i = 10;
    private SpriteRenderer sr;
    private Animator animator;
    private Rigidbody2D rb2d;
    private bool Esta_tocando_suelo = true;
    private bool Puedo_Saltar = false;
    public GameObject Pelota1;
    public GameObject Pelota2;
    public Text Parapuntaje; //Unity Ui
    public int PuntajePersonaje = 0;


    void Start()
    {
        sr = GetComponent<SpriteRenderer>();
        animator = GetComponent<Animator>(); //Rigidbody 
        rb2d = GetComponent<Rigidbody2D>();
        Debug.Log("Hello ! ");
    }

    void Update()
    {
        Parapuntaje.text = "Puntaje : " + PuntajePersonaje;
        if (Input.GetKeyDown(KeyCode.A))
        {
            if (!sr.flipX) //Instantiate(Pelota1.transform);
            {
                // Instantiate(Pelota1, transform.position, Pelota1.transform.rotation);
                var position = new Vector2(transform.position.x + 1.5f, transform.position.y - .10f);
                Instantiate(Pelota1, position, Pelota1.transform.rotation);
            }
            else
            {
                var position = new Vector2(transform.position.x - 4.3f, transform.position.y + .5f);
                Instantiate(Pelota2, position, Pelota2.transform.rotation);
            }
        }


        setA_inactivoAnimator(); //Para setear codigo.
        if (Input.GetKey(KeyCode.RightArrow))
        {
            //Derecha
            sr.flipX = false;
            setA_correrAnimation();
            rb2d.velocity = new Vector2(15, rb2d.velocity.y); //Intanciando vector
        }
        
        if (Input.GetKey(KeyCode.LeftArrow))
        {
            //Izquierda
            sr.flipX = true;
            setA_correrAnimation();
            rb2d.velocity = new Vector2(-10, rb2d.velocity.x);
            Debug.Log("Izquierda ! Hello ¡ ");
        }

        if (Input.GetKey(KeyCode.RightArrow) && Input.GetKey(KeyCode.Space))
        {
            sr.flipX = true;
            //rb2d.velocity=Vector2.up*10; Para saltar
            rb2d.velocity = new Vector2(rb2d.velocity.x, 10);
            setA_SaltarAnimator();
            Debug.Log("Abajo ! Hello ¡ ");
        }

        if (Input.GetKeyUp(KeyCode.LeftArrow) && Input.GetKey(KeyCode.Space))
        {
            //sr.flipX = true;
            setA_SaltarAnimator();
            rb2d.velocity = Vector2.up * 100;
            Debug.Log("Teclado ! Hello ¡ ");
        }

        if (Input.GetKeyDown(KeyCode.Space) && Puedo_Saltar)
        {
            rb2d.velocity = Vector2.up * 30;
            setA_SaltarAnimator();
            Puedo_Saltar = false;
            Debug.Log("Salto ! Hello ¡ ");
        }
        
    }

    private void OnCollisionEnter2D(Collision2D other) 
    {
        // Debug.Log("COLISION  XD!");
        Debug.Log(other.gameObject.layer);
        Debug.Log(other.gameObject.name);
        Esta_tocando_suelo = true; //Solo para el Piso 1 o suelo
        
        if (other.gameObject.layer == 8
        ) //Para no saltar cuando toco el techo y ya no salte--Configuracion LAYER -8 en Pisos
        {
            Debug.Log(other.gameObject.layer);
            Puedo_Saltar = true;
        }

        if (other.gameObject.layer == 3
        ) //Para no saltar cuando toco el techo y ya no salte--Configuracion LAYER -8 en Pisos
        {
            Debug.Log("EndGamae"); //Para finalizar un juego xd
            Puedo_Saltar = true;
            Puedo_Saltar = false;
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag=="SaltoSobreEnemigo")
            IncrementoDePuntaje5();
    }
    private void setA_inactivoAnimator() {
        animator.SetInteger("Estado", value: 0);
    }
    private void setA_correrAnimation() {
        animator.SetInteger("Estado", 1);
    }
    private void setA_SaltarAnimator(){
        animator.SetInteger("Estado", value: 2);
    }
    

    public void IncrementoDePuntaje() { //Para los puntos incremento
        PuntajePersonaje=PuntajePersonaje +10 ;
    }
    public void IncrementoDePuntaje5() { //Para los puntos incremento
        PuntajePersonaje=PuntajePersonaje +5 ;
    }
}


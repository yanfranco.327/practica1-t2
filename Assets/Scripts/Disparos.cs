using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Disparos : MonoBehaviour
{
    private Rigidbody2D rb;
    public float velocityx = 20f;
    private Dog Dog; //Instancio
    // Start is called before the first frame update
    void Start()
    {

        rb = GetComponent<Rigidbody2D>();
        Dog = FindObjectOfType<Dog>(); //Este metodo busca a Adventure en mi personaje.
        Destroy(gameObject,5);
    }

    // Update is called once per frame
    void Update()
    {
        rb.velocity=Vector2.right * velocityx;
        
    }

    //Codigo para eliminar un personaje.
    private void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.tag == "Enemy" ||other.gameObject.tag == "SaltoSobreEnemigo"  )
        {
            Destroy(other.gameObject);
            Destroy(this.gameObject);
            Dog.IncrementoDePuntaje();
        }
     
    }
}
